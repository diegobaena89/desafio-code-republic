import React, { useState } from "react";
import Modal from "react-modal";
import { Input } from "@chakra-ui/react";
import { ChakraProvider } from "@chakra-ui/react";
import { Button } from "@chakra-ui/react";
import { Text } from "@chakra-ui/react";
import { Box } from "@chakra-ui/react";
import "./App.css";

Modal.setAppElement("#root");

function App(props) {
  const [altura, setAltura] = useState(0);
  const [largura, setlargura] = useState(0);
  const [largura2, setlargura2] = useState(0);
  const [largura3, setlargura3] = useState(0);
  const [largura4, setlargura4] = useState(0);
  const [portas, setPortas] = useState(0);
  const [janelas, setJanelas] = useState(0);

  function handlePortas(e) {
    setPortas(e.target.value);
  }

  function handleJanelas(e) {
    setJanelas(e.target.value);
  }

  function handleAltura(e) {
    setAltura(e.target.value);
  }

  function handleLargura(e) {
    setlargura(e.target.value);
  }

  function handleLargura2(e) {
    setlargura2(e.target.value);
  }

  function handleLargura3(e) {
    setlargura3(e.target.value);
  }

  function handleLargura4(e) {
    setlargura4(e.target.value);
  }

  function calculaAmbiente() {
    let litroTinta = 5;
    let parede1 = altura * largura;
    let parede2 = altura * largura2;
    let parede3 = altura * largura3;
    let parede4 = altura * largura4;
    let alturaPortas = 1.52;
    let alturaJanelas = 2.4;

    let areaTotalParede = parede1 + parede2 + parede3 + parede4;
    let totalPortas = alturaPortas * portas;
    let totalJanelas = alturaJanelas * janelas;

    let areaPortaseJanelas = (totalPortas + totalJanelas).toFixed(2);
    let calculoGalao;

    let areaCalculara = areaTotalParede - areaPortaseJanelas;

    calculoGalao = Math.ceil(areaCalculara / litroTinta);

    return calculoGalao;
  }

  function sugestaoGaloes(calculoGalao) {
    let galao;

    if (calculoGalao < 1) {
      return (galao = "sugerimos que você obtenha 2 galões de 0,5");
    } else if (calculoGalao > 1 && calculoGalao < 2.5) {
      return (galao = "Sugerimos que você obtenha 1 galão de 2,5l");
    } else if (calculoGalao > 2.5 && calculoGalao < 3.6) {
      return (galao =
        "sugerimos que você obtenha um galão de 0,5 ou um de 2,5l");
    } else if (calculoGalao > 3.6 && calculoGalao < 18) {
      return (galao =
        "sugerimos que você obtenha um galão de 3,5 ou um de 18l");
    }

    return galao;
  }

  function calculaTamanhosPortasEJanelas() {
    let parede1 = altura * largura;
    let parede2 = altura * largura2;
    let parede3 = altura * largura3;
    let parede4 = altura * largura4;
    let portas = 1.52;
    let janelas = 2.4;

    let areaPortaseJanelas = (portas * janelas).toFixed(2);
    let areaTotalParede = parede1 + parede2 + parede3 + parede4;

    if (altura < portas + 0.3) {
      alert("Altura da parede deve ser superior a altura da porta!");
    }

    if (parede1 < 1 || parede2 < 1 || parede3 < 1 || parede4 < 1) {
      alert("Metragem da parede não pode ser inferior a 1m²");
    }

    if (
      areaPortaseJanelas / 2 > parede1 ||
      areaPortaseJanelas / 2 > parede2 ||
      areaPortaseJanelas / 2 > parede3 ||
      areaPortaseJanelas / 2 > parede4
    ) {
      alert(
        "Área das Portas e Janelas não pode ser superior a 50% da área da parede!"
      );
    }
  }

  function calculaMetragem() {
    let parede1 = altura * largura;
    let parede2 = altura * largura2;
    let parede3 = altura * largura3;
    let parede4 = altura * largura4;
    let portas = 1.52;
    let janelas = 2.4;
    let areaPortaseJanelas = (portas * janelas).toFixed(2);
    let areaTotalParede = parede1 + parede2 + parede3 + parede4;
    let total = areaTotalParede - areaPortaseJanelas;

    return total;
  }

  function mostraRetorno() {
    calculaAmbiente();
    calculaTamanhosPortasEJanelas();
    calculaMetragem();
    sugestaoGaloes();
    handleOpenModel();
  }

  const [modalIsOpen, setModalIsOpen] = useState(false);

  function handleOpenModel() {
    setModalIsOpen(true);
  }

  function handleCloseModal() {
    setModalIsOpen(false);
  }

  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
    },
  };

  return (
    <ChakraProvider>
      <Box m={0} bg="orange.500">
        <Text fontSize="2xl" textAlign="center" color="white" fontWeight="bold">
          Calculo de Tintas
        </Text>
      </Box>
      <Box maxW="700px" mt="2" mx="auto" border="1px" borderColor="orange" p="0" borderRadius="20">
        <Box mb={2} display="flex" p="2">
          <Text textAlign="left" display="flex" margin="0 auto">
            Altura:
          </Text>
          <Input
            variant="flushed"
            type="number"
            name="firstName"
            onChange={handleAltura}
            value={altura}
            ml="60px"
          />
        </Box>
        <Box mb={2} display="flex" bg="orange.300" p="2">
          <Text mr={2} textAlign="left" display="flex" margin="0 5px auto">
            Largura Parede 1
          </Text>
          <Input
            variant="flushed"
            type="number"
            name="firstName"
            onChange={handleLargura}
            value={largura}
          />
        </Box>
        <Box mb={2} display="flex" p="2">
          <Text mr={2} textAlign="left" display="flex" margin="0 5px auto">
            Largura Parede 2
          </Text>
          <Input
            variant="flushed"
            type="number"
            name="firstName"
            onChange={handleLargura2}
            value={largura2}
          />
        </Box>
        <Box mb={2} display="flex" bg="orange.300" p="2">
          <Text mr={2} textAlign="left" display="flex" margin="0 5px auto">
            Largura Parede 3
          </Text>
          <Input
            variant="flushed"
            type="number"
            name="firstName"
            onChange={handleLargura3}
            value={largura3}
          />
        </Box>
        <Box mb={2} display="flex" p="2">
          <Text mr={2} textAlign="left" display="flex" margin="0 5px auto">
            Largura Parede 4
          </Text>
          <Input
            variant="flushed"
            type="number"
            name="firstName"
            onChange={handleLargura4}
            value={largura4}
          />
        </Box>

        <Box mb={2} display="flex" bg="orange.300" p="2">
          <Text mr={2} textAlign="left" display="flex" margin="0 5px auto">
            Nº de Portas:
          </Text>
          <Input
            variant="flushed"
            type="number"
            name="firstName"
            onChange={handlePortas}
            value={portas}
            ml="20px"
          />
        </Box>
        <Box mb={2} display="flex" p="2">
          <Text mr={2} textAlign="left" display="flex" margin="0 5px auto">
            Nº de Janelas:
          </Text>
          <Input
            variant="flushed"
            type="number"
            name="firstName"
            onChange={handleJanelas}
            value={janelas}
            ml="20px"
          />
        </Box>

        <Box>
          <Button
            colorScheme="orange"
            width="100%"
            size="lg"
            onClick={mostraRetorno}
          >
            Calcular
          </Button>
        </Box>
        <Modal
          className="modal"
          isOpen={modalIsOpen}
          onRequestClose={handleCloseModal}
        >
          <button
            onClick={handleCloseModal}
            style={{
              display: "flex",
              fontWeight: "bold",
              flexDirection: "row",
              marginLeft: "98%",
            }}
          >
            X
          </button>
          <div className="div_modal">
            <h2>Olá, seja bem vindo!</h2>
            <p>
              Abaixo você poderá conferir as medidas do seu ambiente e a
              quantidade exata de galões de tinta que você precisa adquirir a
              fim de evitar disperdícios!
            </p>
            <br />
            <hr />
            <br />
            <div>
              {`A metragem para seu ambiente é ${calculaMetragem()} m²`}
              <br />
              {`Você deve adquirir ${calculaAmbiente()} galões de tinta!`}
              <br />
              <p>Para o seu caso,</p>
              {sugestaoGaloes()}
              <p>
                {calculaAmbiente() < 1
                  ? "sugerimos que você obtenha 2 galões de 0,5"
                  : false}
              </p>
              <p>
                {calculaAmbiente() < 2.5
                  ? "Sugerimos que você obtenha 1 galão de 2,5l"
                  : false}
              </p>
              <p>
                {calculaAmbiente() < 3.6
                  ? "sugerimos que você obtenha um galão de 0,5 ou um de 2,5l"
                  : false}
              </p>
              <p>
                {calculaAmbiente() < 18
                  ? "sugerimos que você obtenha um galão de 18l"
                  : false}
              </p>
            </div>
          </div>
        </Modal>
      </Box>
    </ChakraProvider>
  );
}

export default App;
